
* Z3 Internals - Nikolaj Bjorner MSFT

- XMProfiler can be used to gen visuals of search
- E-matching for Q : designed to be quick and fail fast
- model based instantiation is not
- Rustan Boogie - Z3 customer
- MBQI

** Tactics
- is an experimental science, can be categorized according to
  theories, contracting/expanding, translations
- by truth preservations : term rewriting preserves equivalence, some
  preserve sat, some weaken, some strengthen.

** Solvers
- Constrained Horn Solvers called Fixedpoint dolvers for some reason.
- DAGS to store expression, required for linear lookup of terms. Z3
  has all hash cons. LEAN has a continuum of what is and isn't
  hash-consed.
Core <-> Solver interface
EUF handles it.

Model based Filtering: ping pong between EUF and Arith.

Relevancy Filtering: idea: simulate tableau reasoning on top of CDCL.

Ackermann reductions to handle many equalities. A CDCL solver will
hang.

Iterative Deepening : used in strings. Finite func. unfolding. Outer
loop of the sat solver.

** Theories
- EUF - union find
- Egraph can talk to the SAT solver and vice versa.
- EUF internals: nodes contain fields value and boolVar (number SAT
  solver refers to)
- Equality nodes are special, as false value signifies conflict.
- Ematching does matching modulo congruence closures.
- Match g(c,x) with g(b,b)
- root(b)=c and x=b.
- In Z3 : Ematching using code trees.
- code trees were introduced in Vampire.

Slides are online.


* SasyLF - Jon Aldrich CMU
- based on the same metatheory as Twelf.
- used for Lambda Calculus, soundness proofs, etc...
- rigorous, immediate feedback, soundness of asst. is rewarding,
  drives student towards satisfaction.
- come to office with the knowledge that something is wrong, so there
  is a teaching opportunity.
- hindley milner transfers to paper and pencil proofs.
- everything in TAPL can be proved, except (LIMITATION) relational
  arguments.

- terminals, syntax, judgement ...
- judgements look like inference rules.
- make induction analogy : let vs letrec - for keeping vaR in scope.
- requires explicit proofs, but also fills in proofs.
- used for teaching PL, reduces cost of variable binding.


